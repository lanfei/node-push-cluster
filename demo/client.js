var npc = require('../');
var readline = require('readline');

npc.connect('ws://localhost:5223', function (session) {
	console.log('Type something and start your chatting:');

	session.subscribe('test');
	session.on('message', function (channel, data) {
		console.log(data);
	});

	var rl = readline.createInterface(process.stdin, process.stdout);
	rl.on('line', function (line) {
		session.publish('test', line);
	});

});
