var os = require('os');
var cluster = require('cluster');
var npc = require('../');

var port = 5223;

if (cluster.isMaster) {
	var cups = os.cpus();
	for (var i = 0, l = cups.length; i < l; ++i) {
		cluster.fork();
	}
} else {
	var server = npc.createServer({
		//broker: require('npc-redis')(6379, 'localhost')
	});
	server.listen(port);
	server.on('listening', function () {
		console.log('Listening', port);
	});
}
