# node-push-cluster [![NPM version][npm-image]][npm-url]

A scalable realtime push framework.

## Installation

```bash
$ npm install push-cluster
```

## Usage

### Server

```js
var os = require('os');
var cluster = require('cluster');
var npc = require('push-cluster');

var port = 5223;

if (cluster.isMaster) {
	var cups = os.cpus();
	for (var i = 0, l = cups.length; i < l; ++i) {
		cluster.fork();
	}
} else {
	var server = npc.createServer();
	server.listen(port);
	server.on('listening', function () {
		console.log('Listening', port);
	});
}
```

### Client

```js
var npc = require('push-cluster');

npc.connect('ws://localhost:5223', function (session) {

	session.subscribe('test');
	session.publish('test', 'Hello Node Push Cluster');
	session.on('message', function (channel, data) {
		console.log(channel, data);
	});

});

```

### Browser

```js
// Coming soon
```

[npm-url]: https://npmjs.org/package/push-cluster
[npm-image]: https://badge.fury.io/js/push-cluster.svg
